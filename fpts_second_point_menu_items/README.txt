/* $Id: README.txt,v 1.1 2015/10/21 michaelocana Exp $ */

-- SUMMARY --

FPTS Second Point Menu Items feature module creator.

This is for exam purposes only. Feature module to create dummy menus.

-- INSTALLATION --

* To install goto admin/modules. and Enable the FPTS Second Point Menu Items.
* To install goto admin/modules. and Enable the Features.

-- USAGE --

* Refer to the FPTS Second Point module README.txt
