<?php
/**
 * @file
 * fpts_second_point_menu_items.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function fpts_second_point_menu_items_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-fpts-second-point_a-possible-coup-in-saudi-arabia-signals-the-end-of-us-dominance-in-the-mideast:http://www.huffingtonpost.com/david-oualaalou/a-possible-coup-in-saudi_b_8325456.html?ir=Politics.
  $menu_links['menu-fpts-second-point_a-possible-coup-in-saudi-arabia-signals-the-end-of-us-dominance-in-the-mideast:http://www.huffingtonpost.com/david-oualaalou/a-possible-coup-in-saudi_b_8325456.html?ir=Politics'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/david-oualaalou/a-possible-coup-in-saudi_b_8325456.html?ir=Politics',
    'router_path' => '',
    'link_title' => 'A Possible Coup in Saudi Arabia Signals the End of US Dominance in the Mideast',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_a-possible-coup-in-saudi-arabia-signals-the-end-of-us-dominance-in-the-mideast:http://www.huffingtonpost.com/david-oualaalou/a-possible-coup-in-saudi_b_8325456.html?ir=Politics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_politics:<front>',
  );
  // Exported menu link: menu-fpts-second-point_business:<front>.
  $menu_links['menu-fpts-second-point_business:<front>'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Business',
    'options' => array(
      'attributes' => array(
        'title' => 'Business',
      ),
      'identifier' => 'menu-fpts-second-point_business:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-fpts-second-point_did-tim-cook-just-give-us-a-hint-about-apple-cars:http://www.huffingtonpost.com/entry/apple-tim-cook-future-of-cars_562654aae4b08589ef48f3b1?ir=Technology&section=technology.
  $menu_links['menu-fpts-second-point_did-tim-cook-just-give-us-a-hint-about-apple-cars:http://www.huffingtonpost.com/entry/apple-tim-cook-future-of-cars_562654aae4b08589ef48f3b1?ir=Technology&section=technology'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/apple-tim-cook-future-of-cars_562654aae4b08589ef48f3b1?ir=Technology&section=technology',
    'router_path' => '',
    'link_title' => 'Did Tim Cook Just Give Us A Hint About Apple Cars?',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_did-tim-cook-just-give-us-a-hint-about-apple-cars:http://www.huffingtonpost.com/entry/apple-tim-cook-future-of-cars_562654aae4b08589ef48f3b1?ir=Technology&section=technology',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_technology:<front>',
  );
  // Exported menu link: menu-fpts-second-point_doppelgangers-nina-dobrev-and-victoria-justice-cross-paths-at-veuve-clicquot-polo-classic:http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment.
  $menu_links['menu-fpts-second-point_doppelgangers-nina-dobrev-and-victoria-justice-cross-paths-at-veuve-clicquot-polo-classic:http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment',
    'router_path' => '',
    'link_title' => 'Doppelgangers Nina Dobrev And Victoria Justice Cross Paths At Veuve Clicquot Polo Classic',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_doppelgangers-nina-dobrev-and-victoria-justice-cross-paths-at-veuve-clicquot-polo-classic:http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_entertainment:<front>',
  );
  // Exported menu link: menu-fpts-second-point_entertainment:<front>.
  $menu_links['menu-fpts-second-point_entertainment:<front>'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Entertainment',
    'options' => array(
      'attributes' => array(
        'title' => 'Entertainment',
      ),
      'identifier' => 'menu-fpts-second-point_entertainment:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-fpts-second-point_europe-already-beat-its-2020-greenhouse-gas-emissions-target:http://www.huffingtonpost.com/entry/europe-already-beat-its-2020-greenhouse-gas-emissions-target_56263c5ee4b0bce347021b8b?ir=Business&section=business.
  $menu_links['menu-fpts-second-point_europe-already-beat-its-2020-greenhouse-gas-emissions-target:http://www.huffingtonpost.com/entry/europe-already-beat-its-2020-greenhouse-gas-emissions-target_56263c5ee4b0bce347021b8b?ir=Business&section=business'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/europe-already-beat-its-2020-greenhouse-gas-emissions-target_56263c5ee4b0bce347021b8b?ir=Business&section=business',
    'router_path' => '',
    'link_title' => 'Europe Already Beat Its 2020 Greenhouse Gas Emissions Target',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_europe-already-beat-its-2020-greenhouse-gas-emissions-target:http://www.huffingtonpost.com/entry/europe-already-beat-its-2020-greenhouse-gas-emissions-target_56263c5ee4b0bce347021b8b?ir=Business&section=business',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_business:<front>',
  );
  // Exported menu link: menu-fpts-second-point_fall-tv-shows-that-dont-deserve-to-make-it-to-winter:http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment.
  $menu_links['menu-fpts-second-point_fall-tv-shows-that-dont-deserve-to-make-it-to-winter:http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment',
    'router_path' => '',
    'link_title' => 'Fall TV Shows That Don\'t Deserve To Make It To Winter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_fall-tv-shows-that-dont-deserve-to-make-it-to-winter:http://www.huffingtonpost.com/entry/nina-dobrev-victoria-justice-doppelgangers_5625107ee4b08589ef482236?ir=Entertainment&section=entertainment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_entertainment:<front>',
  );
  // Exported menu link: menu-fpts-second-point_get-ready-to-stand-in-line-at-the-department-of-drone-vehicles:http://www.huffingtonpost.com/entry/drone-registry-faa-transportation_56252b3ce4b08589ef4842f6?ir=Technology&section=technology.
  $menu_links['menu-fpts-second-point_get-ready-to-stand-in-line-at-the-department-of-drone-vehicles:http://www.huffingtonpost.com/entry/drone-registry-faa-transportation_56252b3ce4b08589ef4842f6?ir=Technology&section=technology'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/drone-registry-faa-transportation_56252b3ce4b08589ef4842f6?ir=Technology&section=technology',
    'router_path' => '',
    'link_title' => 'Get Ready To Stand In Line At The Department of Drone Vehicles',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_get-ready-to-stand-in-line-at-the-department-of-drone-vehicles:http://www.huffingtonpost.com/entry/drone-registry-faa-transportation_56252b3ce4b08589ef4842f6?ir=Technology&section=technology',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_technology:<front>',
  );
  // Exported menu link: menu-fpts-second-point_heres-what-climate-change-could-do-to-your-diet:http://www.huffingtonpost.com/entry/what-climate-change-could-do-to-your-diet_562670ffe4b0bce347025507?ir=Technology&section=technology.
  $menu_links['menu-fpts-second-point_heres-what-climate-change-could-do-to-your-diet:http://www.huffingtonpost.com/entry/what-climate-change-could-do-to-your-diet_562670ffe4b0bce347025507?ir=Technology&section=technology'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/what-climate-change-could-do-to-your-diet_562670ffe4b0bce347025507?ir=Technology&section=technology',
    'router_path' => '',
    'link_title' => 'Here\'s What Climate Change Could Do To Your Diet',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_heres-what-climate-change-could-do-to-your-diet:http://www.huffingtonpost.com/entry/what-climate-change-could-do-to-your-diet_562670ffe4b0bce347025507?ir=Technology&section=technology',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_technology:<front>',
  );
  // Exported menu link: menu-fpts-second-point_heres-what-really-happens-when-a-restaurant-bans-tipping:http://www.realtor.org/topics/real-estate-today-radio.
  $menu_links['menu-fpts-second-point_heres-what-really-happens-when-a-restaurant-bans-tipping:http://www.realtor.org/topics/real-estate-today-radio'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.realtor.org/topics/real-estate-today-radio',
    'router_path' => '',
    'link_title' => 'Here\'s What Really Happens When A Restaurant Bans Tipping',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_heres-what-really-happens-when-a-restaurant-bans-tipping:http://www.realtor.org/topics/real-estate-today-radio',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_business:<front>',
  );
  // Exported menu link: menu-fpts-second-point_joe-biden-is-either-running-for-president-or-having-a-hell-of-a-time-playing-us:http://www.huffingtonpost.com/entry/joe-biden-president-2016_562647f2e4b0bce3470226c8?ir=Politics&section=politics.
  $menu_links['menu-fpts-second-point_joe-biden-is-either-running-for-president-or-having-a-hell-of-a-time-playing-us:http://www.huffingtonpost.com/entry/joe-biden-president-2016_562647f2e4b0bce3470226c8?ir=Politics&section=politics'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/joe-biden-president-2016_562647f2e4b0bce3470226c8?ir=Politics&section=politics',
    'router_path' => '',
    'link_title' => 'Joe Biden Is Either Running For President Or Having A Hell Of A Time Playing Us',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_joe-biden-is-either-running-for-president-or-having-a-hell-of-a-time-playing-us:http://www.huffingtonpost.com/entry/joe-biden-president-2016_562647f2e4b0bce3470226c8?ir=Politics&section=politics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_politics:<front>',
  );
  // Exported menu link: menu-fpts-second-point_marco-rubios-interview-with-fox-news-may-have-violated-ethics-rules:http://www.huffingtonpost.com/entry/marco-rubio-campaign-funds-ethics-rules_5626b48be4b08589ef498c7a?ir=Politics&section=politics.
  $menu_links['menu-fpts-second-point_marco-rubios-interview-with-fox-news-may-have-violated-ethics-rules:http://www.huffingtonpost.com/entry/marco-rubio-campaign-funds-ethics-rules_5626b48be4b08589ef498c7a?ir=Politics&section=politics'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/marco-rubio-campaign-funds-ethics-rules_5626b48be4b08589ef498c7a?ir=Politics&section=politics',
    'router_path' => '',
    'link_title' => 'Marco Rubio\'s Interview With Fox News May Have Violated Ethics Rules',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_marco-rubios-interview-with-fox-news-may-have-violated-ethics-rules:http://www.huffingtonpost.com/entry/marco-rubio-campaign-funds-ethics-rules_5626b48be4b08589ef498c7a?ir=Politics&section=politics',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_politics:<front>',
  );
  // Exported menu link: menu-fpts-second-point_melanie-griffith-enjoys-family-night-out-with-her-daughters-and-mother:http://www.huffingtonpost.com/entry/melanie-griffith-daughters-tippi-hedren_56263b7fe4b02f6a900dc182?ir=Entertainment&section=entertainment.
  $menu_links['menu-fpts-second-point_melanie-griffith-enjoys-family-night-out-with-her-daughters-and-mother:http://www.huffingtonpost.com/entry/melanie-griffith-daughters-tippi-hedren_56263b7fe4b02f6a900dc182?ir=Entertainment&section=entertainment'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/melanie-griffith-daughters-tippi-hedren_56263b7fe4b02f6a900dc182?ir=Entertainment&section=entertainment',
    'router_path' => '',
    'link_title' => 'Melanie Griffith Enjoys Family Night Out With Her Daughters And Mother\'',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_melanie-griffith-enjoys-family-night-out-with-her-daughters-and-mother:http://www.huffingtonpost.com/entry/melanie-griffith-daughters-tippi-hedren_56263b7fe4b02f6a900dc182?ir=Entertainment&section=entertainment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_entertainment:<front>',
  );
  // Exported menu link: menu-fpts-second-point_politics:<front>.
  $menu_links['menu-fpts-second-point_politics:<front>'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Politics',
    'options' => array(
      'attributes' => array(
        'title' => 'Politics',
      ),
      'identifier' => 'menu-fpts-second-point_politics:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-fpts-second-point_technology:<front>.
  $menu_links['menu-fpts-second-point_technology:<front>'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Technology',
    'options' => array(
      'attributes' => array(
        'title' => 'Technology',
      ),
      'identifier' => 'menu-fpts-second-point_technology:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-fpts-second-point_uber-drivers-rape-sentencing-is-just-the-latest-controversy-for-company:http://www.huffingtonpost.com/entry/uber-driver-sentenced-rape-female-passenger_56264dffe4b0bce347022d25?ir=Business&section=business.
  $menu_links['menu-fpts-second-point_uber-drivers-rape-sentencing-is-just-the-latest-controversy-for-company:http://www.huffingtonpost.com/entry/uber-driver-sentenced-rape-female-passenger_56264dffe4b0bce347022d25?ir=Business&section=business'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'link_path' => 'http://www.huffingtonpost.com/entry/uber-driver-sentenced-rape-female-passenger_56264dffe4b0bce347022d25?ir=Business&section=business',
    'router_path' => '',
    'link_title' => 'Uber Driver\'s Rape Sentencing Is Just The Latest Controversy For Company',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-fpts-second-point_uber-drivers-rape-sentencing-is-just-the-latest-controversy-for-company:http://www.huffingtonpost.com/entry/uber-driver-sentenced-rape-female-passenger_56264dffe4b0bce347022d25?ir=Business&section=business',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-fpts-second-point_business:<front>',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A Possible Coup in Saudi Arabia Signals the End of US Dominance in the Mideast');
  t('Business');
  t('Did Tim Cook Just Give Us A Hint About Apple Cars?');
  t('Doppelgangers Nina Dobrev And Victoria Justice Cross Paths At Veuve Clicquot Polo Classic');
  t('Entertainment');
  t('Europe Already Beat Its 2020 Greenhouse Gas Emissions Target');
  t('Fall TV Shows That Don\'t Deserve To Make It To Winter');
  t('Get Ready To Stand In Line At The Department of Drone Vehicles');
  t('Here\'s What Climate Change Could Do To Your Diet');
  t('Here\'s What Really Happens When A Restaurant Bans Tipping');
  t('Joe Biden Is Either Running For President Or Having A Hell Of A Time Playing Us');
  t('Marco Rubio\'s Interview With Fox News May Have Violated Ethics Rules');
  t('Melanie Griffith Enjoys Family Night Out With Her Daughters And Mother\'');
  t('Politics');
  t('Technology');
  t('Uber Driver\'s Rape Sentencing Is Just The Latest Controversy For Company');

  return $menu_links;
}
