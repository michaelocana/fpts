<?php
/**
 * @file
 * fpts_second_point_menu_items.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function fpts_second_point_menu_items_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-fpts-second-point.
  $menus['menu-fpts-second-point'] = array(
    'menu_name' => 'menu-fpts-second-point',
    'title' => 'FPTS Second Point',
    'description' => 'for testing',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('FPTS Second Point');
  t('for testing');

  return $menus;
}
