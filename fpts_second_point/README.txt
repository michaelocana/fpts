/* $Id: README.txt,v 1.1 2015/10/21 michaelocana Exp $ */

-- SUMMARY --

FPTS First Second module

This is for exam purposes only. Custom module to display dropdown menu.

-- INSTALLATION --

* To install goto admin/modules. and Enable the FPT Second Point module.
* To install goto admin/modules. and Enable the FPTS Second Point Menu Items.
* To install goto admin/modules. and Enable the Features.

-- USAGE --

* To display FPTS Second Point Block Menu goto admin/structure/block.
  Search the "FPTS Second Point Block Menu" block in the Disable list and select
  the Content from the Region drop-down list and save.

* Visit the page to display, please clear cache if no display is showing.
  admin/config/development/performance
