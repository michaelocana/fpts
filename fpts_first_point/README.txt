/* $Id: README.txt,v 1.1 2015/10/21 michaelocana Exp $ */

-- SUMMARY --

FPTS First Point module

This is for exam purposes only. Custom module to display content from web Service.

-- INSTALLATION --

* To install goto admin/modules. and Enable the FPT First Point module.

-- USAGE --

* To enter web service url goto admin/config/development/fpts_first_point.

* To display FPTS First Point custom block goto admin/structure/block.
  Search the "FPTS First Point" block in the Disable list and select 
  which region you want to display the block.

* Visit the page to display, please clear cache if no display is showing.
  admin/config/development/performance