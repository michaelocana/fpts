FPTS Drupal Custom modules. HOW-TO.

--- FOR FIRST POINT -----

FPTS First Point module

This is for exam purposes only. Custom module to display content from web Service.

-- INSTALLATION --

* To install goto admin/modules. and Enable the FPT First Point module.

-- USAGE --

* To enter web service url goto admin/config/development/fpts_first_point.

* To display FPTS First Point custom block goto admin/structure/block.
  Search the "FPTS First Point" block in the Disable list and select 
  which region you want to display the block.

* Visit the page to display, please clear cache if no display is showing.
  admin/config/development/performance
  
---- FOR SECOND POINT ------

FPTS First Second module

This is for exam purposes only. Custom module to display dropdown menu.

-- INSTALLATION --

* To install goto admin/modules. and Enable the FPT Second Point module.
* To install goto admin/modules. and Enable the FPTS Second Point Menu Items.
* To install goto admin/modules. and Enable the Features.

-- USAGE --

* To display FPTS Second Point Block Menu goto admin/structure/block.
  Search the "FPTS Second Point Block Menu" block in the Disable list and select
  the Content from the Region drop-down list and save.

* Visit the page to display, please clear cache if no display is showing.
  admin/config/development/performance